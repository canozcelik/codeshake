## Quick start

Quick start options:

- Clone the repo: `git clone https://bitbucket.org/canozcelik/codeshake.git` or download project.

## Terminal Commands

1. Please install gradle.
2. Install NodeJs.
3. Open terminal.
4. Go to project directory (/codeshake).
5. Run 'gradle bootRun' at terminal.
6. Open new terminal session and go to 'frontend' sub-directory at project directory (/codeshake/frontend).
7. Run 'npm install' command. When it finished run 'npm start' command.
8. Navigate to 'localhost:3000' address 


Thank you