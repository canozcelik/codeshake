import React, { Component } from 'react';
import './App.css';
import Home from './components/Home';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import SynchronizationComponent from './components/SynchronizationComponent';

class App extends Component {
  render() {
    return (
        <Router>
          <Switch>
            <Route path='/' exact={true} component={Home}/>
            <Route path='/sync' exact={true} component={SynchronizationComponent}/>
          </Switch>
        </Router>
    )
  }
}

export default App;