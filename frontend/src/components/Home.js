import React, {Component} from 'react';
import logo from '../logo.png';
import {Button, ButtonGroup} from 'reactstrap';
import {Link} from 'react-router-dom';
import '../App.css';

class Home extends Component {
  state = {
    isLoading: true,
    groups: [],
    showButton: false
  };

  delay(ms) {
    return new Promise(function (resolve, reject) {
      setTimeout(resolve, ms);
    })
  }

  render() {
    const {showButton} = this.state;

    this.delay(2000).then(() => {
      this.setState({showButton: true})
    });

    return (
        <div className="App">
          <header className="App-header">
            <img src={logo} className="rotate-scale-up" alt="logo"/>
            <div className="App-intro">
              <ButtonGroup>
                <Link to="/sync">{showButton ? <Button size="sm" color="light"
                >Start
                  Synchronization</Button> : null}</Link>

              </ButtonGroup>


            </div>
          </header>
        </div>
    );
  }
}

export default Home;