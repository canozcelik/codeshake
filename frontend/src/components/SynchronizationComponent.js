import React, {Component} from 'react';
import 'react-table/react-table.css'
import ReactTable from 'react-table'
import AppNavbar from "./AppNavbar";
import Spinner from "reactstrap/es/Spinner";

class SynchronizationComponent extends Component {

  state = {
    isSyncCompleted: false,
    list: []
  };

  async componentDidMount() {
    const response = await fetch('/codeshake/sync');
    const body = await response.json();
    this.setState({isSyncCompleted: body});
    this.getList();
  }

  getList() {
    fetch('/codeshake/list')
    .then(response => response.json())
    .then(data => this.setState({list: data.users}));
  }

  render() {

    const columns = [{
      Header: 'Id',
      accessor: 'id' // String-based value accessors!
    }, {
      Header: 'Name',
      accessor: 'name'
    }, {
      Header: 'Email',
      accessor: 'email'
    }, {
      Header: 'Role',
      accessor: 'role'
    }];

    const {isSyncCompleted} = this.state;

    return <div>
      <AppNavbar/>
      <div className="App">
        <header className="App-header">

          <div className={isSyncCompleted ? 'hidden' : ''}>
            <Spinner/>
            <div>Data is Synchronizing</div>
          </div>


          <div className={isSyncCompleted ? '' : 'hidden'}>
            <div>Edusake User List</div>
            <ReactTable disabled={true}
                        data={this.state.list}
                        columns={columns}
                        showPagination={true}
                        className="-striped -highlight"
                        defaultPageSize={10}/>
          </div>

        </header>
      </div>
    </div>;

  }

}

export default SynchronizationComponent;
