package org.codeshake.assignment.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties
public class CodeshakeConfig {

  private String url;
  private String token;
  private Endpoints endpoints = new Endpoints();

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public Endpoints getEndpoints() {
    return endpoints;
  }

  public static class Endpoints {

    private Sources sources = new Sources();
    private String familyInformationSystem;
    private String igf;
    private String edushake;

    public Sources getSources() {
      return sources;
    }

    public String getFamilyInformationSystem() {
      return familyInformationSystem;
    }

    public void setFamilyInformationSystem(String familyInformationSystem) {
      this.familyInformationSystem = familyInformationSystem;
    }

    public String getIgf() {
      return igf;
    }

    public void setIgf(String igf) {
      this.igf = igf;
    }

    public String getEdushake() {
      return edushake;
    }

    public void setEdushake(String edushake) {
      this.edushake = edushake;
    }

  }

  public static class Sources {

    private String students;
    private String teachers;

    public String getStudents() {
      return students;
    }

    public void setStudents(String students) {
      this.students = students;
    }


    public String getTeachers() {
      return teachers;
    }

    public void setTeachers(String teachers) {
      this.teachers = teachers;
    }
  }

}
