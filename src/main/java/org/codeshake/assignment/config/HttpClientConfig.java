package org.codeshake.assignment.config;

import com.google.gson.Gson;
import java.security.cert.X509Certificate;
import java.util.Map;
import javax.net.ssl.SSLContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContexts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HttpClientConfig {

  private static Logger logger = LoggerFactory.getLogger(HttpClientConfig.class);

  @Autowired
  private CodeshakeConfig config;

  public HttpResponse doGet(String url, String id) {

    logger.info("[doGet()] Parameters :: url = {}, id = {}", url, StringUtils.stripToNull(id));
    url = StringUtils.isNotBlank(id) ? url + "/" + id : url;
    url += "?" + "token=" + config.getToken();

    HttpGet request = new HttpGet(url);
    HttpResponse response = null;

    try {
      SSLContext sslcontext = SSLContexts.custom()
          .loadTrustMaterial((TrustStrategy) (X509Certificate[] chain, String authType) -> true)
          .build();
      HttpClient httpClient = HttpClientBuilder.create().setSSLContext(sslcontext).build();

      response = httpClient.execute(request);

      logger.info("[doGet()] Response code is {}", response.getStatusLine().getStatusCode());
    } catch (Exception e) {
      logger.error("[doGet()] An Error has been occurred!", e);
    }

    return response;
  }

  public HttpResponse doPost(String url, Map<String, String> params, String id) {

    logger.info("[doPost()] Parameters :: url = {}, params = {}", url, params);
    url += "?" + "token=" + config.getToken();
    if (StringUtils.isNotBlank(id)) {
      url += "&id=" + id;
    }
    HttpPost request = new HttpPost(url);
    request.setHeader("Accept", "application/json");
    request.setHeader("Content-type", "application/json");
    HttpResponse response = null;

    try {
      SSLContext sslcontext = SSLContexts.custom()
          .loadTrustMaterial((TrustStrategy) (X509Certificate[] chain, String authType) -> true)
          .build();
      HttpClient httpClient = HttpClientBuilder.create().setSSLContext(sslcontext).build();
      if (params != null) {
        Gson gson = new Gson();
        StringEntity stringEntity = new StringEntity(gson.toJson(params));
        request.setEntity(stringEntity);
      }

      response = httpClient.execute(request);

      logger.info("[doPost()] Response code is {}", response.getStatusLine().getStatusCode());
    } catch (Exception e) {
      logger.error("[doPost()] An Error has been occurred!", e);
    }

    return response;
  }

}
