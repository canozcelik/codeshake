package org.codeshake.assignment.controller;

import org.codeshake.assignment.model.EdushakeList;
import org.codeshake.assignment.service.EdushakeService;
import org.codeshake.assignment.service.SynchronizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/codeshake")
public class SynchronizationController {

  @Autowired
  private SynchronizeService synchronizeService;

  @Autowired
  private EdushakeService edushakeService;

  @RequestMapping("/sync")
  public boolean sync() {
    synchronizeService.startStudentAndParentSynchronization();
    synchronizeService.startTeacherSynchronization();
    return true;
  }

  @RequestMapping("/list")
  public EdushakeList list() {
    return edushakeService.list();
  }
}
