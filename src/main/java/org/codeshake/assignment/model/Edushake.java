package org.codeshake.assignment.model;

public class Edushake {

  private String id;
  private String name;
  private String email;
  private String intergalacticFederationIdentification;
  private String role;
  private String organization;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getIntergalacticFederationIdentification() {
    return intergalacticFederationIdentification;
  }

  public void setIntergalacticFederationIdentification(
      String intergalacticFederationIdentification) {
    this.intergalacticFederationIdentification = intergalacticFederationIdentification;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public String getOrganization() {
    return organization;
  }

  public void setOrganization(String organization) {
    this.organization = organization;
  }
}
