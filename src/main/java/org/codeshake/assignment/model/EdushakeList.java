package org.codeshake.assignment.model;

import java.util.List;

public class EdushakeList {

  private List<Edushake> users;

  public List<Edushake> getUsers() {
    return users;
  }

  public void setUsers(List<Edushake> users) {
    this.users = users;
  }
}
