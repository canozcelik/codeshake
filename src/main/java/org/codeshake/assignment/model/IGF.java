package org.codeshake.assignment.model;

public class IGF {

  private String intergalacticFederationIdentification;

  public String getIntergalacticFederationIdentification() {
    return intergalacticFederationIdentification;
  }

  public void setIntergalacticFederationIdentification(
      String intergalacticFederationIdentification) {
    this.intergalacticFederationIdentification = intergalacticFederationIdentification;
  }
}
