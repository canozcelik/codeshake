package org.codeshake.assignment.model;

import java.util.HashMap;
import java.util.Map;

public class Person {

  private String id;
  private String name;
  private String email;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Map<String, String> getMap(String intergalacticFederationIdentification, String role) {
    Map<String, String> map = new HashMap<>();
    map.put("id", getId());
    map.put("name", getName());
    map.put("email", getEmail());
    map.put("intergalacticFederationIdentification", intergalacticFederationIdentification);
    map.put("role", role);
    return map;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("{");
    sb.append("id='").append(id).append('\'');
    sb.append(", name='").append(name).append('\'');
    sb.append(", email='").append(email).append('\'');
    sb.append('}');
    return sb.toString();
  }
}
