package org.codeshake.assignment.model;

public class Student extends Person {

  private String parentId;

  public String getParentId() {
    return parentId;
  }

  public void setParentId(String parentId) {
    this.parentId = parentId;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("{");
    sb.append("id='").append(this.getId()).append('\'');
    sb.append(", name='").append(this.getName()).append('\'');
    sb.append(", email='").append(this.getEmail()).append('\'');
    sb.append(", parentId='").append(parentId).append('\'');
    sb.append('}');
    return sb.toString();
  }
}
