package org.codeshake.assignment.model;

import java.util.List;

public class StudentList {

  private List<Student> data;

  public List<Student> getData() {
    return data;
  }

  public void setData(List<Student> data) {
    this.data = data;
  }

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder("StudentList{");
    sb.append("data=").append(data);
    sb.append('}');
    return sb.toString();
  }
}
