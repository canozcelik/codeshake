package org.codeshake.assignment.model;

import java.util.List;

public class TeacherList {

  private List<Teacher> data;

  public List<Teacher> getData() {
    return data;
  }

  public void setData(List<Teacher> data) {
    this.data = data;
  }
}
