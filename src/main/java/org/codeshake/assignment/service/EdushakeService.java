package org.codeshake.assignment.service;

import java.util.Map;
import org.codeshake.assignment.model.EdushakeList;

public interface EdushakeService {

  void save(Map<String, String> map);

  EdushakeList list();

}
