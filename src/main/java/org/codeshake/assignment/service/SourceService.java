package org.codeshake.assignment.service;

import org.codeshake.assignment.model.Parent;
import org.codeshake.assignment.model.StudentList;
import org.codeshake.assignment.model.TeacherList;

public interface SourceService {

  StudentList getStudentList();

  TeacherList getTeacherList();

  Parent getFamilyMember(String id);

}
