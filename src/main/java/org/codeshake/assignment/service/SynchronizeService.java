package org.codeshake.assignment.service;

public interface SynchronizeService {

  void startStudentAndParentSynchronization();

  void startTeacherSynchronization();

}
