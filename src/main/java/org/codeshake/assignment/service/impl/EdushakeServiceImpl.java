package org.codeshake.assignment.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.codeshake.assignment.config.CodeshakeConfig;
import org.codeshake.assignment.config.HttpClientConfig;
import org.codeshake.assignment.model.EdushakeList;
import org.codeshake.assignment.service.EdushakeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EdushakeServiceImpl implements EdushakeService {

  private static Logger logger = LoggerFactory.getLogger(EdushakeServiceImpl.class);

  @Autowired
  private CodeshakeConfig config;

  @Autowired
  HttpClientConfig httpClientConfig;

  @Override
  public void save(Map<String, String> map) {
    Instant start = Instant.now();

    try {
      HttpResponse response = httpClientConfig
          .doPost(config.getUrl() + config.getEndpoints().getEdushake(), map, StringUtils.EMPTY);

      if (response != null && response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
        logger.info("[save()] Registration is completed for this person :: person = {}", map);
      }

      Instant finish = Instant.now();
      long timeElapsed = Duration.between(start, finish).toMillis();
      logger.info("[save()] Elapsed time is {} ms", timeElapsed);
    } catch (Exception e) {
      logger.error("[save()] An error has been occurred at Edushake save method!", e);
    }
  }

  @Override
  public EdushakeList list() {

    Instant start = Instant.now();
    EdushakeList edushakeList = null;

    try {

      HttpResponse response = httpClientConfig
          .doGet(config.getUrl() + config.getEndpoints().getEdushake(),
              StringUtils.EMPTY);

      if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
        edushakeList = new ObjectMapper()
            .readValue(response.getEntity().getContent(), EdushakeList.class);

      }

      Instant finish = Instant.now();
      long timeElapsed = Duration.between(start, finish).toMillis();
      logger.info("[list()] Elapsed time is {} ms", timeElapsed);

    } catch (Exception e) {
      logger.error("[list()] An error has been occurred at Edushake list method!", e);
    }

    return edushakeList;
  }
}
