package org.codeshake.assignment.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.codeshake.assignment.config.CodeshakeConfig;
import org.codeshake.assignment.config.HttpClientConfig;
import org.codeshake.assignment.model.IGF;
import org.codeshake.assignment.service.IGFService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IGFServiceImpl implements IGFService {

  private static Logger logger = LoggerFactory.getLogger(IGFServiceImpl.class);

  @Autowired
  private CodeshakeConfig config;

  @Autowired
  HttpClientConfig httpClientConfig;

  @Override
  public String register(String id) {

    Instant start = Instant.now();
    IGF igf = null;

    try {
      Map<String, String> map = new HashMap<>();
      map.put("id", id);

      HttpResponse response = httpClientConfig
          .doPost(config.getUrl() + config.getEndpoints().getIgf() + "", null, id);

      if (response != null && response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
        igf = new ObjectMapper()
            .readValue(response.getEntity().getContent(), IGF.class);
      }

      Instant finish = Instant.now();
      long timeElapsed = Duration.between(start, finish).toMillis();
      logger.info("[getStudentList()] Elapsed time is {} ms", timeElapsed);

    } catch (Exception e) {
      logger.error("[register()] An error has been occurred!", e);
    }

    return igf != null ? igf.getIntergalacticFederationIdentification() : null;
  }
}
