package org.codeshake.assignment.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.Duration;
import java.time.Instant;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.codeshake.assignment.config.CodeshakeConfig;
import org.codeshake.assignment.config.HttpClientConfig;
import org.codeshake.assignment.model.Parent;
import org.codeshake.assignment.model.StudentList;
import org.codeshake.assignment.model.TeacherList;
import org.codeshake.assignment.service.SourceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SourceServiceImpl implements SourceService {

  private static Logger logger = LoggerFactory.getLogger(SourceServiceImpl.class);

  @Autowired
  private CodeshakeConfig config;

  @Autowired
  HttpClientConfig httpClientConfig;

  @Override
  public StudentList getStudentList() {

    Instant start = Instant.now();
    StudentList studentList = null;

    try {
      HttpResponse response = httpClientConfig
          .doGet(config.getUrl() + config.getEndpoints().getSources().getStudents(),
              StringUtils.EMPTY);

      if (response != null && response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
        studentList = new ObjectMapper()
            .readValue(response.getEntity().getContent(), StudentList.class);
      }

      Instant finish = Instant.now();
      long timeElapsed = Duration.between(start, finish).toMillis();
      logger.info("[getStudentList()] Elapsed time is {} ms", timeElapsed);

    } catch (Exception e) {
      logger.error("[getStudentList()] An error has been occurred!", e);
    }

    return studentList;
  }

  @Override
  public TeacherList getTeacherList() {

    Instant start = Instant.now();
    TeacherList teacherList = null;

    try {
      HttpResponse response = httpClientConfig
          .doGet(config.getUrl() + config.getEndpoints().getSources().getTeachers(),
              StringUtils.EMPTY);

      if (response != null && response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
        teacherList = new ObjectMapper()
            .readValue(response.getEntity().getContent(), TeacherList.class);
      }

      Instant finish = Instant.now();
      long timeElapsed = Duration.between(start, finish).toMillis();
      logger.info("[getTeacherList()] Elapsed time is {} ms", timeElapsed);

    } catch (Exception e) {
      logger.error("[getTeacherList()] An error has been occurred!", e);
    }

    return teacherList;
  }

  @Override
  public Parent getFamilyMember(String id) {

    Instant start = Instant.now();
    Parent parent = null;

    try {
      HttpResponse response = httpClientConfig
          .doGet(config.getUrl() + config.getEndpoints().getFamilyInformationSystem(),
              id);

      if (response != null && response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
        parent = new ObjectMapper().readValue(response.getEntity().getContent(), Parent.class);
      }

      Instant finish = Instant.now();
      long timeElapsed = Duration.between(start, finish).toMillis();
      logger.info("[getFamilyMember()] Elapsed time is {} ms", timeElapsed);

    } catch (Exception e) {
      logger.error("[getFamilyMember()] An error has been occurred!", e);
    }

    return parent;
  }
}
