package org.codeshake.assignment.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.codeshake.assignment.config.HttpClientConfig;
import org.codeshake.assignment.model.Parent;
import org.codeshake.assignment.model.Student;
import org.codeshake.assignment.model.StudentList;
import org.codeshake.assignment.model.Teacher;
import org.codeshake.assignment.model.TeacherList;
import org.codeshake.assignment.service.EdushakeService;
import org.codeshake.assignment.service.IGFService;
import org.codeshake.assignment.service.SourceService;
import org.codeshake.assignment.service.SynchronizeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SynchronizeServiceImpl implements SynchronizeService {

  private static Logger logger = LoggerFactory.getLogger(HttpClientConfig.class);

  public static final String STUDENT = "STUDENT";
  public static final String TEACHER = "TEACHER";
  public static final String PARENT = "PARENT";

  @Autowired
  private SourceService sourceService;

  @Autowired
  private IGFService igfService;

  @Autowired
  private EdushakeService edushakeService;

  @Override
  public void startStudentAndParentSynchronization() {
    StudentList studentList = sourceService.getStudentList();
    if (studentList != null && studentList.getData() != null && !studentList.getData().isEmpty()) {
      for (Student student : studentList.getData()) {
        Parent parent = null;
        if (student != null && StringUtils.isNotBlank(student.getParentId())) {
          parent = sourceService.getFamilyMember(student.getParentId());

          String igfId = igfService.register(student.getId());
          if (StringUtils.isNotBlank(igfId)) {
            edushakeService.save(student.getMap(igfId, STUDENT));
            logger.info(
                "[startStudentAndParentSynchronization()] Edushake registration was completed! :: student = {}",
                student);
          } else {
            logger.warn(
                "[startStudentAndParentSynchronization()] intergalacticFederationIdentification is not registered for this student! :: student = {}",
                student);
          }
        }

        if (parent != null) {

          String igfId = igfService.register(parent.getId());
          if (StringUtils.isNotBlank(igfId)) {
            edushakeService.save(parent.getMap(igfId, PARENT));
            logger.info(
                "[startStudentAndParentSynchronization()] Edushake registration was completed! :: parent = {}",
                parent);
          } else {
            logger.warn(
                "[startStudentAndParentSynchronization()] intergalacticFederationIdentification is not registered for this parent! :: parent = {}",
                parent);
          }

        } else {
          logger.warn(
              "[startStudentAndParentSynchronization()] There is no parent data at source for this student! :: student = {}",
              student);
        }
      }
    } else {
      logger.warn("[startStudentAndParentSynchronization()] There is no student data at source!");
    }
    logger.info("[startStudentAndParentSynchronization()] Registration was completed");
  }

  @Override
  public void startTeacherSynchronization() {
    TeacherList teacherList = sourceService.getTeacherList();

    if (teacherList != null && teacherList.getData() != null && !teacherList.getData().isEmpty()) {
      for (Teacher teacher : teacherList.getData()) {
        String igfId = igfService.register(teacher.getId());
        if (StringUtils.isNotBlank(igfId)) {
          edushakeService.save(teacher.getMap(igfId, TEACHER));
          logger.info(
              "[startTeacherSynchronization()] Edushake registration was completed! :: teacher = {}",
              teacher);
        } else {
          logger.warn(
              "[startTeacherSynchronization()] intergalacticFederationIdentification is not registered for this teacher! :: teacher = {}",
              teacher);
        }
      }
    } else {
      logger.warn("[startTeacherSynchronization()] There is no teacher data at source!");
    }
    logger.info("[startTeacherSynchronization()] Registration was completed");
  }
}
